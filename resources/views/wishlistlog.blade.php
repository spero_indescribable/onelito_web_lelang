@extends('layout.mainlog')

@section('container')

<div class="container p-0">
    <h4 class="m-1">2 <span>Barang</span></h4>
    <div class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3">
        <div class="col">
            <div class="border">
                <a href="/detail_onelito_store"><img src="img/oneli1.png"
                        alt="bio media" class="card-img-top"
                    ></a>
                <div class="px-1">
                    <p class="cb-judul">{!! Illuminate\Support\Str::limit('Bio Tube Bacteria House
                        Media Filter', 100) !!}</p>
                    <p><b>Rp. 1.300.000</b></p>
                    <button class="mb-3 text-danger "
                        style="background-color: transparent;font-size:small;border-color:red"><i
                            class="fa-solid fa-plus"></i>
                        <span>Keranjang</span></button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="border">
                <img src="img/oneli2.png" alt="uniring" class="card-img-top">
                <div class="px-1">
                    <p>Uniring rubber hose /
                        selang aerasi</p>
                    <p><b>Rp. 580.000</b></p>
                    <button class="mb-3 text-danger "
                        style="background-color: transparent;font-size:small;border-color:red"><i
                            class="fa-solid fa-plus"></i> <span>Keranjang</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="border">
                <a href="/detail_onelito_store"><img src="img/oneli1.png"
                        alt="bio media" class="card-img-top"></a>
                <div class="px-1">
                    <p>Bio Tube Bacteria House
                        Media Filter</p>
                    <p><b>Rp. 1.300.000</b></p>
                    <button class="mb-3 text-danger "
                        style="background-color: transparent;font-size:small;border-color:red"><i
                            class="fa-solid fa-plus"></i>
                        <span>Keranjang</span></button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="border">
                <img src="img/oneli2.png" alt="uniring" class="card-img-top">
                <div class="px-1">
                    <p>Uniring rubber hose /
                        selang aerasi</p>
                    <p><b>Rp. 580.000</b></p>
                    <button class="mb-3 text-danger "
                        style="background-color: transparent;font-size:small;border-color:red"><i
                            class="fa-solid fa-plus"></i> <span>Keranjang</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="border">
                <img src="img/oneli2.png" alt="uniring" class="card-img-top">
                <div class="px-1">
                    <p>Uniring rubber hose /
                        selang aerasi</p>
                    <p><b>Rp. 580.000</b></p>
                    <button class="mb-3 text-danger "
                        style="background-color: transparent;font-size:small;border-color:red"><i
                            class="fa-solid fa-plus"></i> <span>Keranjang</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="border">
                <img src="img/oneli2.png" alt="uniring" class="card-img-top">
                <div class="px-1">
                    <p>Uniring rubber hose /
                        selang aerasi</p>
                    <p><b>Rp. 580.000</b></p>
                    <button class="mb-3 text-danger "
                        style="background-color: transparent;font-size:small;border-color:red"><i
                            class="fa-solid fa-plus"></i> <span>Keranjang</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection