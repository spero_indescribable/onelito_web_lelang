@extends('layout.main')

@section('container')
    <style>
        /* .card {
                    width: 100%;
                    height: 100%;
                    display: flex;
                    justify-content: space-between;
                    align-items: flex-start;
                    flex-direction: column;
                } */

        .cb-judul {
            height: 6rem;

        }

        #img-ikan {
            /* background-size: cover;
                       background-repeat: no-repeat;
                        height: 100%;
                        width: 100%; */

            /* width: 100%;
            height: 23vw;
            object-fit: cover; */

            height: 310px;

        }
    </style>
    <div class="container">
        <h5>Rules Auction</h5>
        <p class="m-0">Halo member lelangkoi.co.id</p>
        <p class="m-0">Saat Ini sedang berlangsung 🥳🥳
            SPECIAL DEALER AUCTION </p>
        <p class="m-0">*(Schedule Lelang: 9-11 November 2022)
            CLOSING TIME TODAY AT 21.00 WIB*</p>
        <p class="m-0">Langsung dilihat koleksi-koleksi ikan lelangnya..
            Jangan sampai kelewatan🥳🥳</p>
        <p>*CLICK LINK BELOW :
            Link : https://lelangkoi.co.id/auction/*</p>
        <p class="m-0">Hormat Kami</p>
        <p>Tim Lelangkoi.co.id</p>

        <div class="my-5">
            <p style="color: red"></p>
        </div>

        <div class="container-fluid">
            <div>
                
            </div>

            <div class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3 mb-5">
                <div class="col mt-3">
                    <div class="card">
                        <img src="img/koi11.jpg" class="card-img-top" alt="..." style="height: 310px">
                        <div class="card-body">
                            <div class="cb-judul">
                                <h5 class="card-title">Jenis ikan | Parent Fish | Pedigree | Size | Farm</h5>
                            </div>
                            <p class="m-0">Number of bids</p>

                            <div class="row">
                                <div class="col-6">
                                    <p class="" style="color: red">15</p>
                                </div>

                                <div class="col-6 p-0">
                                    <div class="row">
                                        <div class="col-4 p-0 px-1 text-end">
                                            <i style="color:red" class="fa-solid fa-caret-down"></i>
                                        </div>
                                        <div class="col-8 p-0 pt-1">
                                            <p class="m-0" style="font-size:70%;color:red">HIGHEST BID</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="font-size:80%">Harga saat ini</p>
                                    <p class="m-0" style="color: red;font-size:75%">Rp. 7.500.000</p>
                                </div>

                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="text-align: end;font-size: 80%">Countdown</p>
                                    <p class="m-0" style="text-align: end;color :red;font-size:75%;">00:35:45</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-6 p-0 px-sm-2">
                                    <a href="/login" class="btn btn-danger w-100 d-flex justify-content-between p-1"
                                        style="font-size: 80%">BID NOW <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-6 col-md-6 pe-0 px-sm-2">
                                    <a href="/login"
                                        class="btn btn-secondary w-100 d-flex justify-content-between px-1 p-1"
                                        style="font-size: 80%">DETAIL <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-9 p-0">
                                    <a href="/login" class="btn btn-light w-100 d-flex justify-content-between">VIDEO
                                        <span><i class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-3 p-0">
                                    <button class="border-0 m-1" style="background-color: transparent;font-size:larger"><i
                                            class="far fa-heart"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col mt-3">
                    <div class="card">
                        <img src="img/koi12.jpg" class="card-img-top" alt="..." id="img-ikan">
                        <div class="card-body">
                            <div class="cb-judul">
                                <h5 class="card-title">Jenis ikan | Parent Fish | Pedigree | Size | Farm</h5>
                            </div>
                            <p class="m-0">Number of bids</p>
                            <p class="" style="color: red">7</p>
                            <div class="row">
                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="font-size:80%">Harga saat ini</p>
                                    <p class="m-0" style="color: red;font-size:75%">Rp. 6.500.000</p>
                                </div>
                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="text-align: end;font-size: 80%">Countdown</p>
                                    <p class="m-0" style="text-align: end;color :red;font-size:75%;">00:35:45</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-6 p-0 px-sm-2">
                                    <a href="/login" class="btn btn-danger w-100 d-flex justify-content-between p-1"
                                        style="font-size: 80%">BID NOW <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-6 col-md-6 pe-0 px-sm-2">
                                    <a href="/login"
                                        class="btn btn-secondary w-100 d-flex justify-content-between px-1 p-1"
                                        style="font-size: 80%">DETAIL <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-9 p-0">
                                    <a href="/login" class="btn btn-light w-100 d-flex justify-content-between">VIDEO
                                        <span><i class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-3 p-0">
                                    <button class="border-0 m-1" style="background-color: transparent;font-size:larger"><i
                                            class="far fa-heart"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col mt-3">
                    <div class="card">
                        <img src="img/koi12.jpg" id="img-ikan" class="card-img-top" alt="...">
                        <div class="card-body">
                            <div class="cb-judul">
                                <h5 class="card-title">Jenis ikan | Parent Fish | Pedigree | Size | Farm</h5>
                            </div>
                            <p class="m-0">Number of bids</p>
                            <p class="" style="color: red">11</p>
                            <div class="row">
                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="font-size:80%">Harga saat ini</p>
                                    <p class="m-0" style="color: red;font-size:75%">Rp. 5.000.000</p>
                                </div>

                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="text-align: end;font-size: 80%">Countdown</p>
                                    <p class="m-0" style="text-align: end;color :red;font-size:75%;">00:35:45</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-6 p-0 px-sm-2">
                                    <a href="/login" class="btn btn-danger w-100 d-flex justify-content-between p-1"
                                        style="font-size: 80%">BID NOW <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-6 col-md-6 pe-0 px-sm-2">
                                    <a href="/login"
                                        class="btn btn-secondary w-100 d-flex justify-content-between px-1 p-1"
                                        style="font-size: 80%">DETAIL <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-9 p-0">
                                    <a href="/login" class="btn btn-light w-100 d-flex justify-content-between">VIDEO
                                        <span><i class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-3 p-0">
                                    <button class="border-0 m-1" style="background-color: transparent;font-size:larger"><i
                                            class="far fa-heart"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col mt-3">
                    <div class="card">
                        <img src="img/koi12.jpg" class="card-img-top" alt="..." style="height: 310px">
                        <div class="card-body">
                            <div class="cb-judul">
                                <h5 class="card-title">Jenis ikan | Parent Fish | Pedigree | Size | Farm</h5>
                            </div>
                            <p class="m-0">Number of bids</p>
                            <p class="" style="color: red">8</p>
                            <div class="row">
                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="font-size:80%">Harga saat ini</p>
                                    <p class="m-0" style="color: red;font-size:75%">Rp. 4.000.000</p>
                                </div>

                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="text-align: end;font-size: 80%">Countdown</p>
                                    <p class="m-0" style="text-align: end;color :red;font-size:75%;">00:35:45</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-6 p-0 px-sm-2">
                                    <a href="/login" class="btn btn-danger w-100 d-flex justify-content-between p-1"
                                        style="font-size: 80%">BID NOW <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-6 col-md-6 pe-0 px-sm-2">
                                    <a href="/login"
                                        class="btn btn-secondary w-100 d-flex justify-content-between px-1 p-1"
                                        style="font-size: 80%">DETAIL <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-9 p-0">
                                    <a href="/login" class="btn btn-light w-100 d-flex justify-content-between">VIDEO
                                        <span><i class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-3 p-0">
                                    <button class="border-0 m-1" style="background-color: transparent;font-size:larger"><i
                                            class="far fa-heart"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col mt-3">
                    <div class="card">
                        <img src="img/koi12.jpg" class="card-img-top" alt="..." style="height: 310px">
                        <div class="card-body">
                            <div class="cb-judul">
                                <h5 class="card-title">Jenis ikan | Parent Fish | Pedigree | Size | Farm</h5>
                            </div>
                            <p class="m-0">Number of bids</p>
                            <p class="" style="color: red">11</p>
                            <div class="row">
                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="font-size:80%">Harga saat ini</p>
                                    <p class="m-0" style="color: red;font-size:75%">Rp. 5.000.000</p>
                                </div>

                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="text-align: end;font-size: 80%">Countdown</p>
                                    <p class="m-0" style="text-align: end;color :red;font-size:75%;">00:35:45</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-6 p-0 px-sm-2">
                                    <a href="/login" class="btn btn-danger w-100 d-flex justify-content-between p-1"
                                        style="font-size: 80%">BID NOW <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-6 col-md-6 pe-0 px-sm-2">
                                    <a href="/login"
                                        class="btn btn-secondary w-100 d-flex justify-content-between px-1 p-1"
                                        style="font-size: 80%">DETAIL <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-9 p-0">
                                    <a href="/login" class="btn btn-light w-100 d-flex justify-content-between">VIDEO
                                        <span><i class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-3 p-0">
                                    <button class="border-0 m-1" style="background-color: transparent;font-size:larger"><i
                                            class="far fa-heart"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col mt-3">
                    <div class="card">
                        <img src="img/koi12.jpg" class="card-img-top" alt="..." style="height: 310px">
                        <div class="card-body">
                            <div class="cb-judul">
                                <h5 class="card-title">Jenis ikan | Parent Fish | Pedigree | Size | Farm</h5>
                            </div>
                            <p class="m-0">Number of bids</p>
                            <p class="" style="color: red">8</p>
                            <div class="row">
                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="font-size:80%">Harga saat ini</p>
                                    <p class="m-0" style="color: red;font-size:75%">Rp. 4.000.000</p>
                                </div>

                                <div class="col-6 p-0 px-lg-2">
                                    <p class="m-0" style="text-align: end;font-size: 80%">Countdown</p>
                                    <p class="m-0" style="text-align: end;color :red;font-size:75%;">00:35:45</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 col-md-6 p-0 px-sm-2">
                                    <a href="/login" class="btn btn-danger w-100 d-flex justify-content-between p-1"
                                        style="font-size: 80%">BID NOW <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-6 col-md-6 pe-0 px-sm-2">
                                    <a href="/login"
                                        class="btn btn-secondary w-100 d-flex justify-content-between px-1 p-1"
                                        style="font-size: 80%">DETAIL <span><i
                                                class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-9 p-0">
                                    <a href="/login" class="btn btn-light w-100 d-flex justify-content-between">VIDEO
                                        <span><i class="fa-solid fa-circle-chevron-right"></i></span></a>
                                </div>
                                <div class="col-3 p-0">
                                    <button class="border-0 m-1" style="background-color: transparent;font-size:larger"><i
                                            class="far fa-heart"></i></button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
