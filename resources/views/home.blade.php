@extends('layout.main')

@section('container')
    <style>
        /* On screens that are 992px or less, set the background color to blue */
        @media screen and (min-width: 601px) {
            .nav-atas {
                display: none
            }
        }

        /* On screens that are 600px or less, set the background color to olive */
        @media screen and (max-width: 600px) {
            .nav-samping {
                display: none;
            }

        }

        .cb-judul {
            height: 5rem;

        }

        .cb-judu {
            height: 3.5rem;
        }

        .cb-jud {
            height: 2.5rem;
        }
    </style>



    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner img-mh-300">
            <div class="carousel-item active">
                <div class="container-fluit" style="background-color:red;">
                    <img src="img/Frame.png" class="d-block w-100" alt="Frame">
                </div>
            </div>
            <div class="carousel-item">
                <div class="container-fluit" style="background-color:red;">
                    <img src="img/Frame.png" class="d-block w-100" alt="Frame">
                </div>
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>

    <div class="container mt-3 mt-lg mt-lg-5">

    </div>

    <div class="container nav-atas">
        <div class="row">
            <div class="col-6 col-lg-3 mt-3">
                <div class="card">
                    <img src="img/koi11.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <div class="cb-jud">
                            <h5 class="card-title">Jenis ikan | Parent Fish | Pedigree | Size | Farm</h5>
                        </div>
                        <p style="font-size: 10px" class="card-text ma">Starting Price</p>
                        <p style="color :red;font-size: 12px" class="m-0">Rp. 10.500.000</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3 mt-3">
                <div class="card">
                    <img src="img/koi12.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <div class="cb-jud">
                            <h5 style="font-size: 10px" class="card-title">Jenis ikan | Parent Fish | Pedigree | Size | Farm
                            </h5>
                        </div>
                        <p style="font-size: 10px" class="card-text ma">Starting Price</p>
                        <p style="color :red;font-size: 12px" class="m-0">Rp. 5.500.000</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3 mt-3">
                <div class="card">
                    <img src="img/koi11.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <div class="cb-jud">
                            <h5 style="font-size: 10px" class="card-title">Jenis ikan | Parent Fish | Pedigree | Size | Farm
                            </h5>
                        </div>
                        <p style="font-size: 10px" class="card-text ma">Starting Price</p>
                        <p style="color :red;font-size: 12px" class="m-0">Rp. 8.500.000</p>
                    </div>
                </div>
            </div>
            <div class="col-6 col-lg-3 mt-3">
                <div class="card">
                    <img src="img/koi12.jpg" class="card-img-top" alt="...">
                    <div class="card-body">
                        <div class="cb-jud">
                            <h5 style="font-size: 10px" class="card-title">Jenis ikan | Parent Fish | Pedigree | Size | Farm
                            </h5>
                        </div>
                        <p style="font-size: 10px" class="card-text ma">Starting Price</p>
                        <p style="color :red;font-size: 12px" class="m-0">Rp. 4.500.000</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container nav-samping">
        <div class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3">
            <div class="col">
                <div class="card">
                    <img src="img/koi_3.jpg" class="card-img-top" alt="..." style="height: 312px">
                    <div class="card-body">
                        <div class="cb-judul">
                            <h5 class="card-title">{!! Illuminate\Support\Str::limit('Jenis ikan | Parent Fish | Pedigree | Size | Farm sfsefsefwefaw', 55) !!}</h5>
                        </div>
                        <p class="card-text ma">Starting Price</p>
                        <p style="color :red">Rp. 10.500.000</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <img src="img/koi12.jpg" class="card-img-top" alt="..." style="height: 312px">
                    <div class="card-body">
                        <div class="cb-judul">
                            <h5 class="card-title">{!! Illuminate\Support\Str::limit('Jenis ikan | Parent Fish | Pedigree | Size | Farm', 55) !!}</h5>
                        </div>
                        <p class="card-text ma">Starting Price</p>
                        <p style="color :red">Rp. 5.500.000</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <img src="img/koi11.jpg" class="card-img-top" alt="..." style="height: 312px">
                    <div class="card-body">
                        <div class="cb-judul">
                            <h5 class="card-title">{!! Illuminate\Support\Str::limit('Jenis ikan | Parent Fish | Pedigree | Size | Farm', 55) !!}</h5>
                        </div>
                        <p class="card-text ma">Starting Price</p>
                        <p style="color :red">Rp. 8.500.000</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <img src="img/koi12.jpg" class="card-img-top" alt="..." style="height: 312px">
                    <div class="card-body">
                        <div class="cb-judul">
                            <h5 class="card-title">{!! Illuminate\Support\Str::limit('Jenis ikan | Parent Fish | Pedigree | Size | Farm', 55) !!}</h5>
                        </div>
                        <p class="card-text ma">Starting Price</p>
                        <p style="color :red">Rp. 4.500.000</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <img src="img/koi12.jpg" class="card-img-top" alt="..." style="height: 312px">
                    <div class="card-body">
                        <div class="cb-judul">
                            <h5 class="card-title">{!! Illuminate\Support\Str::limit('Jenis ikan | Parent Fish | Pedigree | Size | Farm', 55) !!}</h5>
                        </div>
                        <p class="card-text ma">Starting Price</p>
                        <p style="color :red">Rp. 4.500.000</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5">
    </div>



    <div class="container nav-atas">
        <div class="d-flex overflow-scroll">
            <div class="p-1">
                <div class="p-3 border bg-light" style="width: 208px">
                    <a href="/login">
                        <img src="img/bio_media.png" alt="bio media" class="card-img-top"
                            style=" height: 166;width: 166;">
                    </a>
                    <div class="cb-jud">
                        <p>{!! Illuminate\Support\Str::limit(
                            'Bio Tube Bacteria House Media Filter ldukfysidufosiedfgyosyi widuuduwauiowiowiwiiwi',
                            70,
                        ) !!}</p>
                    </div>
                    <p><b>Rp. 1.300.000</b></p>
                    <div class="row">
                        <div class="col-6 p-0">
                            <button class="border-0 btn-success rounded-2" style="background-color:#188518;">Order
                                Now</button>
                        </div>
                        <div class="col-2 m-auto">
                            <button class="border-4 rounded" style="background-color: red;border-color:red"><i
                                    class="fa-solid fa-cart-shopping" style="color: white"></i></button>
                        </div>
                        <div class="col-2 m-auto">
                            <button class="border-0" style="background-color: transparent"><i class="far fa-heart"
                                    style="font-size: x-large"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-1">
                <div class="p-3 border bg-light" style="width: 200px">
                    <a href="/login">
                        <img src="img/uniring.jpeg" alt="uniring" class="card-img-top"
                            style=" height: 166;width: 166;">
                    </a>
                    <div class="cb-jud">
                        <p>{!! Illuminate\Support\Str::limit('Uniring rubber hose / selang aerasi', 70) !!}</p>
                    </div>
                    <p><b>Rp. 580.000</b></p>
                    <div class="row">
                        <div class="col-6 p-0">
                            <button class="border-0 btn-success rounded-2" style="background-color:#188518;">Order
                                Now</button>
                        </div>
                        <div class="col-2 m-auto">
                            <button class="border-4 rounded" style="background-color: red;border-color:red"><i
                                    class="fa-solid fa-cart-shopping" style="color: white"></i></button>
                        </div>
                        <div class="col-2 m-auto">
                            <button class="border-0" style="background-color: transparent"><i class="far fa-heart"
                                    style="font-size: x-large"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-1">
                <div class="p-3 border bg-light" style="width: 200px">
                    <a href="/login">
                        <img src="img/selang.jpg" alt="selang" class="card-img-top"
                            style="width: 166px;height: 166px;">
                    </a>
                    <div class="cb-jud">
                        <p>{!! Illuminate\Support\Str::limit('Bio Tube Bacteria House Media Filter', 70) !!}</p>
                    </div>
                    <p><b>Rp. 90.000</b></p>
                    <div class="row">
                        <div class="col-6 p-0">
                            <button class="border-0 btn-success rounded-2" style="background-color:#188518;">Order
                                Now</button>
                        </div>
                        <div class="col-2 m-auto">
                            <button class="border-4 rounded" style="background-color: red;border-color:red"><i
                                    class="fa-solid fa-cart-shopping" style="color: white"></i></button>
                        </div>
                        <div class="col-2 m-auto">
                            <button class="border-0" style="background-color: transparent"><i class="far fa-heart"
                                    style="font-size: x-large"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-1">
                <div class="p-3 border bg-light" style="width: 200px">
                    <a href="/login">
                        <img src="img/bak_ukur.jpg" alt="bak_ukur" class="card-img-top"
                            style=" height: 166;width: 166;">
                    </a>
                    <div class="cb-jud">
                        <p>Mistar ukur koi /
                            bak ukur</p>
                    </div>
                    <p><b>Rp. 600.000</b></p>
                    <div class="row">
                        <div class="col-6 p-0">
                            <button class="border-0 btn-success rounded-2" style="background-color:#188518;">Order
                                Now</button>
                        </div>
                        <div class="col-2 m-auto">
                            <button class="border-4 rounded" style="background-color: red;border-color:red"><i
                                    class="fa-solid fa-cart-shopping" style="color: white"></i></button>
                        </div>
                        <div class="col-2 m-auto">
                            <button class="border-0" style="background-color: transparent"><i class="far fa-heart"
                                    style="font-size: x-large"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="p-1">
                <div class="p-3 border bg-light" style="width: 200px">
                    <a href="/login">
                        <img src="img/matala.jpg" alt="matala" class="card-img-top" style=" height: 166;width: 166;">
                    </a>
                    <div class="cb-jud">
                        <p>{!! Illuminate\Support\Str::limit('Matala Abu Media Filter Mekanik', 70) !!}</p>
                    </div>
                    <p><b>Rp. 974.000</b></p>
                    <div class="row">
                        <div class="col-6 p-0">
                            <button class="border-0 btn-success rounded-2" style="background-color:#188518;">Order
                                Now</button>
                        </div>
                        <div class="col-2 m-auto">
                            <button class="border-4 rounded" style="background-color: red;border-color:red"><i
                                    class="fa-solid fa-cart-shopping" style="color: white"></i></button>
                        </div>
                        <div class="col-2 m-auto">
                            <button class="border-0" style="background-color: transparent"><i class="far fa-heart"
                                    style="font-size: x-large"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="class nav-samping">
        <div class="container">
            <div class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3">
                <div class="col">
                    <div class="p-3 border bg-light">
                        <a href="/detail_onelito_store"><img src="img/bio_media.png" alt="bio media"
                                class="card-img-top" height="170"></a>
                        <div class="cb-judu">
                            <p>{!! Illuminate\Support\Str::limit('Bio Tube Bacteria House Media Filter ldukfysidufosiedfgyosyi', 45) !!}</p>
                        </div>
                        <p><b>Rp. 1.300.000</b></p>
                        <div class="row">
                            <div class="col-md-6 d-grid p-0">
                                <button class="border-0 btn-success rounded-2" style="background-color:#188518;">Order
                                    Now</button>
                            </div>
                            <div class="col-md-3 m-auto">
                                <button class="border-4 rounded" style="background-color: red;border-color:red"><i
                                        class="fa-solid fa-cart-shopping" style="color: white"></i></button>
                            </div>
                            <div class="col-md-3 m-auto">
                                <button class="border-0" style="background-color: transparent"><i class="far fa-heart"
                                        style="font-size: x-large"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="p-3 border bg-light">
                        <img src="img/uniring.jpeg" alt="uniring" class="card-img-top" height="170">
                        <div class="cb-judu">
                            <p>{!! Illuminate\Support\Str::limit('Uniring rubber hose /selang aerasi', 45) !!}</p>
                        </div>
                        <p><b>Rp. 580.000</b></p>
                        <div class="row">
                            <div class="col-md-6 d-grid p-0">
                                <button class="border-0 btn-success rounded-2" style="background-color:#188518;">Order
                                    Now</button>
                            </div>
                            <div class="col-md-3 m-auto">
                                <button class="border-4 rounded" style="background-color: red;border-color:red"><i
                                        class="fa-solid fa-cart-shopping" style="color: white"></i></button>
                            </div>
                            <div class="col-md-3 m-auto">
                                <button class="border-0" style="background-color: transparent"><i class="far fa-heart"
                                        style="font-size: x-large"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="p-3 border bg-light">
                        <img src="img/selang.jpg" alt="selang" class="card-img-top" height="170">
                        <div class="cb-judu">
                            <p>{!! Illuminate\Support\Str::limit('Bio Tube Bacteria House Media Filter', 45) !!}</p>
                        </div>
                        <p><b>Rp. 90.000</b></p>
                        <div class="row">
                            <div class="col-md-6 d-grid p-0">
                                <button class="border-0 btn-success rounded-2" style="background-color:#188518;">Order
                                    Now</button>
                            </div>
                            <div class="col-md-3 m-auto">
                                <button class="border-4 rounded" style="background-color: red;border-color:red"><i
                                        class="fa-solid fa-cart-shopping" style="color: white"></i></button>
                            </div>
                            <div class="col-md-3 m-auto">
                                <button class="border-0" style="background-color: transparent"><i class="far fa-heart"
                                        style="font-size: x-large"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="p-3 border bg-light">
                        <img src="img/bak_ukur.jpg" alt="bak_ukur" class="card-img-top" height="170">
                        <div class="cb-judu">
                            <p>{!! Illuminate\Support\Str::limit('Mistar ukur koi / bak ukur', 45) !!}</p>
                        </div>
                        <p><b>Rp. 600.000</b></p>
                        <div class="row">
                            <div class="col-md-6 d-grid p-0">
                                <button class="border-0 btn-success rounded-2" style="background-color:#188518;">Order
                                    Now</button>
                            </div>
                            <div class="col-md-3 m-auto">
                                <button class="border-4 rounded" style="background-color: red;border-color:red"><i
                                        class="fa-solid fa-cart-shopping" style="color: white"></i></button>
                            </div>
                            <div class="col-md-3 m-auto">
                                <button class="border-0" style="background-color: transparent"><i class="far fa-heart"
                                        style="font-size: x-large"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="p-3 border bg-light">
                        <img src="img/matala.jpg" alt="matala" class="card-img-top" height="170">
                        <div class="cb-judu">
                            <p>{!! Illuminate\Support\Str::limit('Matala Abu Media Filter Mekanik', 45) !!}</p>
                        </div>
                        <p><b>Rp. 974.000</b></p>
                        <div class="row">
                            <div class="col-md-6 d-grid p-0">
                                <button class="border-0 btn-success rounded-2" style="background-color:#188518;">Order
                                    Now</button>
                            </div>
                            <div class="col-md-3 m-auto">
                                <button class="border-4 rounded" style="background-color: red;border-color:red"><i
                                        class="fa-solid fa-cart-shopping" style="color: white"></i></button>
                            </div>
                            <div class="col-md-3 m-auto">
                                <button class="border-0" style="background-color: transparent"><i class="far fa-heart"
                                        style="font-size: x-large"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5 text-center">
        <h3>ONELITO <span style="color:red;">KOI</span></h3>
        <br>
    </div>
    <div class="container">
        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dictum a tellus tortor vulputate
            sodales. Et scelerisque a, rutrum elit. Quam nulla tortor nunc lacus. Odio sit id sollicitudin nibh orci sed
            egestas. Diam, sit mi, et pharetra in ut augue tristique quis. Diam sed dapibus adipiscing nulla amet et aliquet
            auctor</p>
        <p class="text-center"> Dolor, in et, cursus id felis sit lacus. In tristique nullam sed magna proin lacinia amet.
            Viverra sed lectus eu nam.Justo, leo massa enim, et felis aenean.</p>

        <p class="text-center mb-5">Volutpat risus accumsan feugiat in et id egestas. Sed morbi tristique nunc arcu.
            Lobortis tortor in lectus tellus non, pretium viverra. Nibh mattis condimentum consectetur ut facilisi fermentum
            mattis aliquam. </p>
    </div>







    <div class="container">
        <div class="justify-content-around row">
            <div class="border col-lg-2 col-9 mt-4">
                <div class="">
                    <p class="style text-center"><i class="fa-solid fa-envelope" style="color: red"></i></p>
                    <p class="style text-center"><b>Email</b></p>
                    <p class="style text-center">onelito.koi@gmail.com</p>
                </div>
            </div>
            <div class="border col-lg-2 col-9 mt-4">
                <div class="">
                    <p class="style text-center"><i class="fa-solid fa-bag-shopping" style="color: red"></i></p>
                    <p class="style text-center"><b>Tokopedia</b></p>
                    <a href="https://www.tokopedia.com/onelitokoi">
                        <p class="style text-center">onelitokoi</p>
                    </a>
                </div>
            </div>
            <div class="border col-lg-2 col-9 mt-4">
                <div class="">
                    <p class="style text-center"><i class="fas fa-map-marker-alt" style="color: red"></i></p>
                    <p class="style text-center"><b>Address</b></p>
                    <p class="style text-center">Jl. Tandon Ciater D No. 50, BSD, Ciater, Serpong Sub-District, South
                        Tangerang City Banten 15310</p>
                </div>
            </div>
            <div class="border col-lg-2 col-9 mt-4">
                <div class="">
                    <p class="style text-center"><i class="fas fa-phone" style="color: red"></i></p>
                    <p class="style text-center"><b>Contact Us</b></p>
                    <p class="style text-center">0811-972-857</p>
                    <p class="style text-center">0811-972-857</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluit m-5">
    </div>

    <div class="container">
        <div class="row row-cols-2 row-cols-lg-5 g-2 g-lg-3">
            <div class="col mt-3">
                <div class="card">
                    <img src="img/koi_2.jpg" class="card-img-top" alt="..." style="height: 310px">
                    <div class="m-2 me-auto">
                        <h5 class="card-title">{!! Illuminate\Support\Str::limit('32nd Champion', 18) !!}</h5>
                        <p class="card-text ma">Tahun : 2015</p>
                        <p>Size : 50 cm</p>
                    </div>
                </div>
            </div>
            <div class="col mt-3">
                <div class="card">
                    <img src="img/koi_2.jpg" class="card-img-top" alt="..." style="height: 310px">
                    <div class="m-2 me-auto">
                        <h5 class="card-title">{!! Illuminate\Support\Str::limit('Matala Abu Media Filter Mekanik', 18) !!}</h5>
                        <p class="card-text ma">Tahun : 2015</p>
                        <p>Size : 50 cm</p>
                    </div>
                </div>
            </div>
            <div class="col mt-3">
                <div class="card">
                    <img src="img/koi_2.jpg" class="card-img-top" alt="..." style="height: 310px">
                    <div class="m-2 me-auto">
                        <h5 class="card-title">
                            {!! Illuminate\Support\Str::limit('32nd Champion', 18) !!}
                        </h5>
                        <p class="card-text ma">Tahun : 2015</p>
                        <p>Size : 50 cm</p>
                    </div>
                </div>
            </div>
            <div class="col mt-3">
                <div class="card">
                    <img src="img/koi11.jpg" class="card-img-top" alt="..." style="height: 310px">
                    <div class="m-2 me-auto">
                        <h5 class="card-title">
                            {!! Illuminate\Support\Str::limit('32nd Champion', 18) !!}
                        </h5>
                        <p class="card-text ma">Tahun : 2015</p>
                        <p>Size : 50 cm</p>
                    </div>
                </div>
            </div>
            <div class="col mt-3">
                <div class="card">
                    <img src="img/koi12.jpg" class="card-img-top" alt="..." style="height: 310px">
                    <div class="m-2 me-auto">
                        <h5 class="card-title">
                            {!! Illuminate\Support\Str::limit('32nd Champion', 18) !!}
                        </h5>
                        <p class="card-text ma">Tahun : 2015</p>
                        <p>Size : 50 cm</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container my-5">
        <a href="/login" style="color: red">Lihat lebih Banyak >></a>
    </div>
@endsection
