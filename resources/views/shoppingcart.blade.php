@extends('profil')

@section('container')
    <div class="container my-3 text-center">
        <h5><i class="fa-solid fa-cart-shopping"></i> <b>Shopping cart</b></h5>
    </div>
    <div class="container">
        <div class="container border-top border-bottom py-3">
            <input class="form-check-input" style="font-size:large" type="checkbox" value="" id="Pilih Semua">
            <label class="form-check-label" for="Pilih Semua">
                Pilih Semua
            </label>
        </div>

        <div class="container">
            <div class="container d-flex p-0 my-3">
                <input class="form-check-input mr-3 my-auto" type="checkbox" value="" id="flexCheckDefault">
                <div class="card mr-3">
                    <a href="/detail_onelito_store"><img src="img/bio_media.png" class="card-img-top"
                            style="height: 10vh; width: 25vw; object-fit: cover;" alt="..."></a>
                </div>
                <div>
                    <p class="m-0">Bio Tube Bacteria
                        House
                        Media Filter</p>
                    <p class="m-0"><b>Rp. 1.300.000</b></p>
                </div>
            </div>
            <div class="container d-flex p-0 my-3 justify-content-between">
                <p class="my-auto text-danger">Tulis Catatan</p>
                <p class="my-auto text-center">
                    Pindahkan ke Wishlist |
                </p>
                <button class="border-0" style="background-color: transparent"><i
                        class="fa-regular fa-trash-can"></i></button>
                <div class="btn-group" role="group" aria-label="Basic outlined example">
                    <button type="button" class="border-0 btn-light mr-2" style="background-color:tranparent">
                        <i class="fa-sharp fa-solid fa-circle-minus text-black-50" style="font-size: larger"></i>
                    </button>
                    <h1> 1 </h1>
                    <button type="button" class=" border-0 btn-light ml-2">
                        <i class="fa-solid fa-circle-plus text-danger" style="font-size: larger"></i>
                    </button>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="container d-flex p-0 my-3">
                <input class="form-check-input mr-3 my-auto" type="checkbox" value="" id="flexCheckDefault">
                <div class="card mr-3">
                    <a href="/detail_onelito_store"><img src="img/uniring.jpeg" class="card-img-top"
                            style="height: 10vh; width: 25vw; object-fit: cover;" alt="..."></a>
                </div>
                <div>
                    <p class="m-0">Uniring rubber hose
                        /selang aerasi</p>
                    <p class="m-0"><b>Rp. 580.000</b></p>
                </div>
            </div>
            <div class="container d-flex p-0 my-3 justify-content-between">
                <p class="my-auto text-danger">Tulis Catatan</p>
                <p class="my-auto text-center">
                    Pindahkan ke Wishlist |
                </p>
                <button class="border-0" style="background-color: transparent"><i
                        class="fa-regular fa-trash-can"></i></button>
                <div class="btn-group" role="group" aria-label="Basic outlined example">
                    <button type="button" class="border-0 btn-light mr-2" style="background-color:tranparent">
                        <i class="fa-sharp fa-solid fa-circle-minus text-black-50" style="font-size: larger"></i>
                    </button>
                    <h1> 1 </h1>
                    <button type="button" class=" border-0 btn-light ml-2">
                        <i class="fa-solid fa-circle-plus text-danger" style="font-size: larger"></i>
                    </button>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="container d-flex p-0 my-3">
                <input class="form-check-input mr-3 my-auto" type="checkbox" value="" id="flexCheckDefault">
                <div class="card mr-3">
                    <a href="/detail_onelito_store"><img src="img/bio_media.png" class="card-img-top"
                            style="height: 10vh; width: 25vw; object-fit: cover;" alt="..."></a>
                </div>
                <div>
                    <p class="m-0">Bio Tube Bacteria
                        House
                        Media Filter</p>
                    <p class="m-0"><b>Rp. 1.300.000</b></p>
                </div>
            </div>
            <div class="container d-flex p-0 my-3 justify-content-between">
                <p class="my-auto text-danger">Tulis Catatan</p>
                <p class="my-auto text-center">
                    Pindahkan ke Wishlist |
                </p>
                <button class="border-0" style="background-color: transparent"><i
                        class="fa-regular fa-trash-can"></i></button>
                <div class="btn-group" role="group" aria-label="Basic outlined example">
                    <button type="button" class="border-0 btn-light mr-2" style="background-color:tranparent">
                        <i class="fa-sharp fa-solid fa-circle-minus text-black-50" style="font-size: larger"></i>
                    </button>
                    <h1> 1 </h1>
                    <button type="button" class=" border-0 btn-light ml-2">
                        <i class="fa-solid fa-circle-plus text-danger" style="font-size: larger"></i>
                    </button>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="container d-flex p-0 my-3">
                <input class="form-check-input mr-3 my-auto" type="checkbox" value="" id="flexCheckDefault">
                <div class="card mr-3">
                    <a href="/detail_onelito_store"><img src="img/uniring.jpeg" class="card-img-top"
                            style="height: 10vh; width: 25vw; object-fit: cover;" alt="..."></a>
                </div>
                <div>
                    <p class="m-0">Uniring rubber hose
                        /selang aerasi</p>
                    <p class="m-0"><b>Rp. 580.000</b></p>
                </div>
            </div>
            <div class="container d-flex p-0 my-3 justify-content-between">
                <p class="my-auto text-danger">Tulis Catatan</p>
                <p class="my-auto text-center">
                    Pindahkan ke Wishlist |
                </p>
                <button class="border-0" style="background-color: transparent"><i
                        class="fa-regular fa-trash-can"></i></button>
                <div class="btn-group" role="group" aria-label="Basic outlined example">
                    <button type="button" class="border-0 btn-light mr-2" style="background-color:tranparent">
                        <i class="fa-sharp fa-solid fa-circle-minus text-black-50" style="font-size: larger"></i>
                    </button>
                    <h1> 1 </h1>
                    <button type="button" class=" border-0 btn-light ml-2">
                        <i class="fa-solid fa-circle-plus text-danger" style="font-size: larger"></i>
                    </button>
                </div>
            </div>
        </div>
        <hr>
        <div class="container">
            <div class="container d-flex p-0 my-3">
                <input class="form-check-input mr-3 my-auto" type="checkbox" value="" id="flexCheckDefault">
                <div class="card mr-3">
                    <a href="/detail_onelito_store"><img src="img/bio_media.png" class="card-img-top"
                            style="height: 10vh; width: 25vw; object-fit: cover;" alt="..."></a>
                </div>
                <div>
                    <p class="m-0">Bio Tube Bacteria
                        House
                        Media Filter</p>
                    <p class="m-0"><b>Rp. 1.300.000</b></p>
                </div>
            </div>
            <div class="container d-flex p-0 my-3 justify-content-between">
                <p class="my-auto text-danger">Tulis Catatan</p>
                <p class="my-auto text-center">
                    Pindahkan ke Wishlist |
                </p>
                <button class="border-0" style="background-color: transparent"><i
                        class="fa-regular fa-trash-can"></i></button>
                <div class="btn-group" role="group" aria-label="Basic outlined example">
                    <button type="button" class="border-0 btn-light mr-2" style="background-color:tranparent">
                        <i class="fa-sharp fa-solid fa-circle-minus text-black-50" style="font-size: larger"></i>
                    </button>
                    <h1> 1 </h1>
                    <button type="button" class=" border-0 btn-light ml-2">
                        <i class="fa-solid fa-circle-plus text-danger" style="font-size: larger"></i>
                    </button>
                </div>
            </div>
        </div>


        <div class="container border-top fixed-bottom d-flex p-3 justify-content-between bg-white">
            <div class="my-auto">
                <h5 class="">Total Harga</h5>
                <h5 class="font-bold">Rp. 0</h5>
            </div>
            <a class="btn btn-secondary w-25" href="/transaksi" role="button">Pesan
                Sekarang</a>
        </div>
    </div>
@endsection
